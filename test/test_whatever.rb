require 'minitest/unit'
require 'minitest/autorun'
require 'minitest/pride'

require_relative '../lib/whatever.rb'

class TestWhatever < Minitest::Unit::TestCase
  def test_is_a_two
    assert_equal 2, 2
  end

  def test_is_a_three
    assert_equal 3, Whatever.is_a_two
  end

  def test_is_a_four
    assert_equal 4, Whatever.is_a_two
  end

  def test_is_a_five
    assert_equal 5, 5
  end

  def test_is_a_six
    assert_equal 6, Whatever.is_a_two
  end

  def test_is_an_eight
    assert_equal 7, Whatever.is_a_two
  end

  def test_is_a_skip
    skip
  end
end
